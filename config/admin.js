module.exports = ({ env }) => ({
  auth: {
    secret: env("ADMIN_JWT_SECRET", "ADMIN_JWT_SECRET"),
  },
  apiToken: {
    salt: env("API_TOKEN_SALT", "ADMIN_JWT_SECRET"),
  },
});
